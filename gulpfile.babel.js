'use strict';

import plugins       from 'gulp-load-plugins';
import yargs         from 'yargs';
import browser       from 'browser-sync';
import gulp          from 'gulp';
import nunjucks      from 'gulp-nunjucks-render';
import rimraf        from 'rimraf';
import yaml          from 'js-yaml';
import fs            from 'fs';
import webpackStream from 'webpack-stream';
import webpack2      from 'webpack';
import named         from 'vinyl-named';
import autoprefixer  from 'autoprefixer';
import Fiber         from 'fibers';
import path          from 'path';

const $ = plugins({
  postRequireTransforms: {
    sass: (sass) => sass(require('sass'))
  }
});

const PRODUCTION = !!(yargs.argv.production);

const { PORT, PATHS } = loadConfig();
const faviconConfig = require(PATHS.faviconConfig);

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

function clean(done) {
  rimraf(PATHS.dist, done);
}

function copy() {
  return gulp.src(PATHS.assets)
    .pipe(gulp.dest(PATHS.dist + '/assets'));
}

function pages() {
  return gulp.src('src/pages/**/*.njk')
    .pipe($.data(file => {
      const filename = path.basename(file.path, '.njk')
      return JSON.parse(
        fs.readFileSync(`src/data/${filename}.json`, 'utf8')
      )
    }))
    .pipe(
      nunjucks({
        path: [
          'src/layouts/',
          'src/pages/',
          'src/partials/'
        ]
      })
    )
    .pipe(gulp.dest(PATHS.dist));
}

function beautify() {
  return gulp.src(PATHS.dist + '/**/*.html')
    .pipe($.prettyHtml({
      indent_size: 2
    }))
    .pipe(gulp.dest(PATHS.dist));
}

function generateFavicon(done) {
  $.realFavicon.generateFavicon(Object.assign({}, faviconConfig, {
    masterPicture: 'src/assets/img/favicon.png',
    dest: PATHS.dist,
    markupFile: PATHS.faviconData
  }), function() {
    done();
  });
}

function injectFavicon() {
  const faviconData = JSON.parse(fs.readFileSync(PATHS.faviconData, 'utf8')).favicon.html_code;

  return gulp.src(PATHS.dist + '/**/*.html')
    .pipe($.realFavicon.injectFaviconMarkups(faviconData))
    .pipe(gulp.dest(PATHS.dist));
}

function sass() {
  const postCssPlugins = [
    autoprefixer()
  ].filter(Boolean);

  return gulp.src('src/assets/scss/app.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: PATHS.sass,
      fiber: Fiber
    })
      .on('error', $.sass.logError))
    .pipe($.postcss(postCssPlugins))
    .pipe($.if(PRODUCTION, $.cleanCss({ compatibility: 'ie9' })))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/assets/css'))
    .pipe(browser.reload({ stream: true }));
}

function javascript() {
  return gulp.src(PATHS.entries)
    .pipe(named())
    .pipe($.sourcemaps.init())
    .pipe(webpackStream({config: require("./webpack.config")}, webpack2))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/assets/js'));
}

function images() {
  return gulp.src('src/assets/img/**/*')
    .pipe($.if(PRODUCTION, $.imagemin([
      $.imagemin.mozjpeg({ progressive: true }),
    ])))
    .pipe(gulp.dest(PATHS.dist + '/assets/img'));
}

function server(done) {
  browser.init({
    server: PATHS.dist, port: PORT
  }, done);
}

function reload(done) {
  browser.reload();
  done();
}

function watch() {
  gulp.watch(PATHS.assets, copy);
  gulp.watch('src/{layouts,pages,partials}/**/*.njk').on('all', gulp.series(pages, reload));
  gulp.watch('src/data/**/*.{js,json,yml}').on('all', gulp.series(pages, reload));
  gulp.watch('src/assets/scss/**/*.scss').on('all', sass);
  gulp.watch('src/assets/js/**/*.js').on('all', gulp.series(javascript, reload));
  gulp.watch('src/assets/img/**/*').on('all', gulp.series(images, reload));
}

const build = gulp.series(clean, gulp.parallel(pages, javascript, images, copy), sass);
const favicon = gulp.series(clean, pages, copy, generateFavicon, injectFavicon, beautify);

module.exports = {
  copy,
  pages,
  favicon,
  sass,
  javascript,
  images,
  watch,
  build
};
module.exports.default = gulp.series(build, server, watch);
